# Описание

Приложение, разворачивающееся в кластере Kubernetes через Helm Chart, с использованием CI/CD.
Файлы для развертывания облачной инфраструктуры находятся в отдельном проекте:
https://gitlab.com/sergei-ageev/terraform_yandex_cloud

Ниже указаны действия, проделанные для развертывания инфраструктуры и запуска приложения.

## Установка K8S
1. Подключаем YC на АРМ - https://cloud.yandex.ru/docs/cli/quickstart
2. Создаем Yandex Object Storage. Вводим имя "tf-state-ageev", всё остальное по умолчанию. Жмем "Создать бакет".
3. В настройках бакета создаем ключ KMS для шифрования.
4. Создаем YDB. Пишем имя "tf-state-ageev-ydb", тип базы данных "serveless".
5. В YDB создаем таблицу:
- Пишем имя - table-lockid.
- Тип таблицы - документная.
- Колонка - имя LockID, Тип String, Ключ партицирования - ставим галку.
6. Создаем сервисный аккаунт через YC и выдаем ему роли для работы с YDB и Object Storage:
```
yc iam service-account create --name sa-ts-gitlab
yc resource-manager folder add-access-binding --service-account-name sa-ts-gitlab --role storage.uploader my-folder
yc resource-manager folder add-access-binding --service-account-name sa-ts-gitlab --role ydb.editor my-folder
yc resource-manager folder add-access-binding --service-account-name sa-ts-gitlab --role kms.keys.encrypterDecrypter my-folder
```
Список ролей - https://cloud.yandex.ru/docs/iam/concepts/access-control/roles
7. Создаем ключ для сервисного аккуаунта для работы с YDB и Object Storage:
```
yc iam access-key create --service-account-name sa-ts-gitlab
```
Записываем значения access_key и secret_key.
8. В файле terraform/providers.tf добавляем следующую информацию:
- bucket: "tf-state-ageev"
- dynamodb_endpoint: "значение dynamodb_endpoint прописываем ссылку из настроек YDB - Endpoint"
- dynamodb_table: "table-lockid"
- access_key: "<access_key из 8 пункта>"
- secret_key: "<secret_key из 8 пункта>"
- token: "<получаем с помощью команды yc config get token>"
- cloud_id: "<получаем с помощью команды yc config get cloud-id>"
- folder_id: "<получаем с помощью команды yc config get folder-id>"
9. Заполняем файл terraform/terraform.tfvars.
10. Ставим Kubernetes и 1 рабочий узел с помощью terraform.
Команды вводим с АРМа. Переходим в папку с файлами .tf и применяем:
```
terraform apply
```

# Установка и настройка Gitlab
1. Разворачиваем Managed Service for Gitlab через web-интерфейс:
- Подсеть та же, что и у k8s.
- Остальные данные выбираем по своему усмотрению.
Подробнее: https://cloud.yandex.ru/docs/managed-gitlab/quickstart?from=int-console-empty-state
2. На почту придет ссылка для смены пароля администратора в Gitlab - меняем.
3. Производим дополнительные настройки безопасности Gitlab:
- Отключаем возможность регистрации новых пользователей: Settings/General/Sigh-up restrictions.
  Снимаем галку Sign-up enabled.
- Отключаем возможность взаимодействия с репозиторием по https:
  Settings/Visability and access controls/Enabled Git access protocols - Only SSH
- Сохраняем настройки.

## Установка Gitlab Runner
1. Создаем группу в Gitlab с именем final-project. Visability - Private.
2. Переходим в созданную группу. Открываем в левом меню CICD/Runners и справа жмем кнопку
"Register a group runner". Выписываем Registration Token.
3. Отключаем функцию Auto DevOps в настройках CI/CD.
4. Установку Gitlab Runner в кластере производим по инструкии с помощью Helm:
https://cloud.yandex.ru/docs/managed-kubernetes/operations/applications/gitlab-runner#helm-install
Команда для установки:
```
export HELM_EXPERIMENTAL_OCI=1 && \
helm pull oci://cr.yandex/yc-marketplace/yandex-cloud/gitlab-org/gitlab-runner/chart/gitlab-runner \
  --version 0.49.1-8 \
  --untar && \
helm install \
  --namespace gitlab-runner \
  --create-namespace \
  --set gitlabDomain=https://gitlab-ageev.gitlab.yandexcloud.net \
  --set runnerRegistrationToken=XXXXXXXXXXXXXXXX \
  gitlab-runner ./gitlab-runner/ \
  --atomic

runnerRegistrationToken - необходимо подставить свой.
```

# Настройка CI/CD Terraform
1. Создаем пустой проект в gitlab с именем terraform в группе final-project.
2. Делаем копию файла providers.tf
```
cp providers.tf providers.tf.bak
```
Providers.tf.bak остается для локального использования terraform в случае ЧП.
3. Переносим значения из файла providers.tf в переменные в проекте Gitlab:
- Значение access_key вставляем в переменную AWS_ACCESS_KEY_ID.
- Значение secret_key вставляем в переменную AWS_SECRET_ACCESS_KEY.
4. Из файла providers.tf удаляем token, access_key, secret_key.
6. Создаем сервисный аккаунт для работы Gitlab + Terraform с нужной ролью.
```
yc iam service-account create --name sa-t-gitlab
yc resource-manager folder add-access-binding --service-account-name sa-t-gitlab --role editor my-folder
```
7. Выгружаем ключ сервисного аккаунта в файл. Содержимое файла загружаем
в Gitlab в переменную YC_KEY.
```
cd ~
yc iam key create --service-account-name sa-t-gitlab --output sa-key.json
``` 
6. Локальную папку terraform добавляем в проект terraform в gitlab:
```
cd terraform
git init --initial-branch=main
git remote add origin git@gitlab-ageev.gitlab.yandexcloud.net:final-project/terraform.git
git add .
git commit -m "Initial commit"
git push -u origin main
```
# Настройка CICD graduation_work в Kubernetes

1. Делаем token для CICD с именем gitlab-deploy-token с правами read_registry и write_registry.
Подробнее https://docs.gitlab.com/ee/user/project/deploy_tokens/#gitlab-deploy-token
Settings/Repository/Deploy - Tokens
2. На АРМе создаем namespace "yelb" в кластере K8S.
```
kubectl create ns yelb
```
3. Создаем сервисный аккаунт для CI/CD в namespace "yelb".
```
kubectl create serviceaccount yelb-sa --namespace=yelb
```
4. Создаем роль, описывающую разрешения для сервисного аккаунта.
Для этого переходим в папку k8s.
```
cd graduation_work/k8s
kubectl apply -f yelb-cicd-role.yml
```
5. Создаем RoleBinding в namespace "yelb" и привязываем роль, созданную в 4 пункте, с сервисным
аккаунтом, созданном в 3 пункте.
```
kubectl create rolebinding yelb-cicd-rb --role=yelb-cicd-role --serviceaccount=yelb:yelb-sa  --namespace=yelb
```
6. Для запуска deploy в проект graduation_work добавляем следующие переменные CICD:
- K8S_API_URL
- K8S_CERT_AUTHORITY_DATA
- K8S_CLUSTER_NAME
- K8S_SA_CICD
- K8S_TOKEN
На основе данных переменных будет сгенерирована конфигурация для подключения к API K8S кластера.

# Установка PostgreSQL и настройка
1. Создаем Managed Service for PostgreSQL через terraform.
Данные для базы данных добавляем через переменные CICD:
- TF_VAR_postgresql_db
- TF_VAR_postgresql_user
- TF_VAR_postgresql_password
3. Добавляем в переменные CICD проекта graduation_work
имя БД, логин, пароль от базы данных. Переменные:
- POSTGRESQL_DB
- POSTGRESQL_USER
- POSTGRESQL_PASSWORD
4. Создаем таблицу в базе данных с помощью SQL запросов через веб-интерфейс.
```
CREATE TABLE restaurants (
    	name        char(30),
    	count       integer,
    	PRIMARY KEY (name)
	);
	INSERT INTO restaurants (name, count) VALUES ('outback', 0);
	INSERT INTO restaurants (name, count) VALUES ('bucadibeppo', 0);
	INSERT INTO restaurants (name, count) VALUES ('chipotle', 0);
	INSERT INTO restaurants (name, count) VALUES ('ihop', 0);
```
5. В значении yelb-db.databaseIP в values helm чарта прописываем ip базы данных.
 
# Установка Ingress Controller
1. Делегируем купленное доменное имя на NS сервера Яндекс.
https://cloud.yandex.ru/docs/dns/concepts/dns-zone#public-zones
2. Создаем публичный static IP адрес для LoadBalancer с помощью publicip.tf.
3. Редактируем dns.tf: добавляем dns зону и A запись - имя домена и внешний ip, полученный в пункте 2.
Необходимо подумать, как перенести белый IP средствами terraform.
4. В файл /graduation_work/k8s/values.yaml добавляем ExternalIPs
(ip адреса worker node, на которые будет балансироваться
трафик с сетевого балансировщка перед ingress controller).
4. Устанавливаем Ingress Controller с настройками в папке k8s/values.yaml.
```
cd k8s/ingress-controller
helm upgrade --install ingress-nginx ingress-nginx/ingress-nginx -f values.yaml
```
При установке Ingress Controller в Яндекс будет автоматически создана целевая группа
для сетевого балансировщика нагрузки!
5. Вручную через WEB создаем Network Load Balancer с привязкой целевой группы из пункта 4.
```
Тип: внешний
Публичный айпи: Список - выбираем айпи из пункта 2.
Обработчик 1: Порт 80 / Целевой порт 80
Обработчик 2: Порт 443 / Целевой порт 443
Целевая группа: из пункта 4. Проверка состояния - выбираем тип TCP с портом 80.
```

# Установка и настройка Cert Manager
1. Устанавливаем Cert Manager
```
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.9.1/cert-manager.yaml
```
2. В файле k8s/acme-issuer.yaml добавляем свою почту.
3. Применяем k8s/acme-issuer.yaml
```
cd k8s/cert-manager
kubectl apply -f acme-issuer.yaml
```

# Настраиваем Ingress
1. В значении domainName в helm chart прописываем свой домен.

# Создание и добавление в репозиторий образа с yamllint
```
cd yamllint
docker login gitlab-ageev.gitlab.yandexcloud.net:5050
docker build -t gitlab-ageev.gitlab.yandexcloud.net:5050/final-project/graduation_work/yamllint:1.0 .
docker push gitlab-ageev.gitlab.yandexcloud.net:5050/final-project/graduation_work/yamllint:1.0
```

Всё настроено. Кодим, отправляем в удаленнный git репозиторий и наблюдаем за 
CI/CD Pipeline.
