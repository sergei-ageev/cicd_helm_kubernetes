FROM alpine:3.19.0

RUN apk update && \
    apk add --no-cache postgresql-client

ENTRYPOINT ["/bin/sh", "-c", "while true; do sleep 5; done"]